

#### Ansibleの使い方

```
1. brew install ansible でインストール

2. カレントディレクトリに ansible.cfg と hosts ファイルを作成。
   各種設定をansible.cfg に書き、hostsに接続先サーバのIPを書く。

3. yml で Playbookを書く。

4. ansible-playbook <playbook.yml> を打つと、playbook内の処理が接続先サーバで実行される。
```


#### ssh Agent について

```
・ ssh agent を利用すると、リモートサーバ上でもローカルの鍵を利用してgit clone や pullが可能になる。この仕組みをansible内でも使用できる。

・ `$ ssh-add ~/.ssh/id_rsa `  を打つと、ansible.cfgの以下の部分が有効になる。

-------------------- ansible.cfg

[ssh_connection]
ssh_args = -o ForwardAgent=yes

--------------------

```

```
既に鍵が登録されているかどうかは、以下のコマンドで確認できる。

$ ssh-add -l
```

```
ssh -A でec2-userでログイン後、rootユーザーで gitをpullする場合、sudo su - する際に下記コマンドでssh Agentのauthの値を渡す必要があるので注意する。

$ sudo su -l -c "export SSH_AUTH_SOCK=$SSH_AUTH_SOCK; bash"
```


参考ページ: [http://takemaru123.hatenablog.jp/entry/20121128/1354123091](http://takemaru123.hatenablog.jp/entry/20121128/1354123091)
