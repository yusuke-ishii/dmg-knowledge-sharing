
# JSON.stringifyの注意点

* JSON.stringifyの引数にJSONオブジェクト以外のものを渡すと、エラーにならず文字列になる。
* 例えば、nullを渡すと'null'という文字列になる。

```
> JSON.stringify(null)
'null'

> JSON.stringify(0)
'0'

> JSON.stringify(false)
'false'

```

* nullオブジェクトをJSON.stringifyしたら null になるか例外発生するだろうと想定しているとバグにつながるので注意しましょう。

