
# jsの真偽値判定

|value	| type	| result|
|---|---|---|
|{}	|オブジェクト	| true|
|"hoge"	|文字列	|true|
|""	|文字	|false|
|1|	数値|	true|
|-1|	数値|	true|
|0|	数値|	false|
|[]|	配列|	true|
|true|	真偽値|	true|
|false|	真偽値|	false|
|undefined|	undefined|	false|
|null|	null|false |

*[引用元Qiitaページ](https://qiita.com/phi/items/723aa59851b0716a87e3)


# 例

* 下記のコードで真偽値判定をテストできる。

```
> ([]) ? console.log('is_true') : console.log('is_false')
is_true

> ([] === true) ? console.log('is_true') : console.log('is_false')
is_false

> (![] === true) ? console.log('is_true') : console.log('is_false')
is_false

> ![]
false

> (!![] === true) ? console.log('is_true') : console.log('is_false')
is_true


```
* nodeがインストールされていれば、 `$ node` コマンドでREPLを起動できる。(`.exit`を打つと終了できる)
* 等価演算子はできるだけ == ではなく === を使うことが望ましい。


