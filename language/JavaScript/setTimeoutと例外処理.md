# setTimeoutと例外処理


下記のsetTimeoutのコールバック関数内でthrowされたErrorは、
catchの中に入ってこない。

```
try {

  setTimeout(function() {
    throw new Error('error');
  }, 3000);

} catch(e) {
  console.log(e); // ここで捕捉できない
}
```

なので、Errorをcatchするには、下記のように書き換える必要がある。

```

setTimeout(function() {
  try {
    throw new Error('error');
  } catch(e) {
    console.log(e);
  }
}, 3000);

```
