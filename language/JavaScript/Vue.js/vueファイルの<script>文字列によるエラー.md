

# vueファイルの`</script>`文字列によるエラー

### 事象
```
拡張子.vue ファイルの中の`<script>`タグ内で、
`</script>`という文字を含む文字列を記述するとエラーが発生する。
```

サンプルコード
```
let bodyScript = `
    <script>
      let test = 'test';
    </script>   <= ここのせいでエラー
`;
```

---

### 解決法
* `</script>`文字列のスラッシュの前にバックスラッシュを入れてエスケープする

サンプルコード
```
let bodyScript = `
    <script>
      let test = 'test';
    <\/script>  <= ここにバックスラッシュを入れる
`;
```


