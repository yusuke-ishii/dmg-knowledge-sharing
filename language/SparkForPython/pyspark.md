# Pythonでspark

##pyspark
SparkのPython用API。
sparkをインストールするとbinに入ってくるが、実態はsparkをimportした状態のpython shell。<br>
直接python側でimportもできるが、動作が安定しない。(installしたsparkと別の物をimportしてる所為だと思われる)

###findspark
上記課題を若干強引に解決するライブラリ。<br>
実行時にsys.pathにpysparkを追加することでsparkをライブラリとして読み込んでいる。
```
import findspark
findspark.init（）
from pyspark.sql import SparkSession
spark = SparkSession.builder().getOrCreate()
```

## spark-submit
spark-submitはSpark アプリケーションを実行するコマンドで、ファイル名やその他のオプションを指定して実行できる。
ここで指定できるオプションは後述するsparkSessionで指定できるオプションとほぼ同じである。

```
$ spark-submit 実行ファイルパス
```

|options|内容|
|---|---|
|--master|動作モード。省略した場合は local モード。詳細は下記 Master URL の表を参照。|
|--class|Java または Scala の main メソッドが実装されているアプリケーションのクラス。|
|--name|アプリケーション名。Spark の Web UI に表示される。|
|--deploy-mode|	ドライバの起動先を Client か Cluster かを指定する。デフォルトは Client。<br>YARN の場合は```--master```で指定するため通常使用しない。|
|--jars|ドライバやエグゼキュータのクラスパスに追加したい JAR ファイルを指定。<br>通常は uber-JAR を使用するのが一般的。（カンマ区切り）|
|--files|エグゼキュータにファイルを配布（カンマ区切り）|
|--conf|SparkConf の設定オプションを```プロパティ名=値```の形式で指定する。<br>```spark.*```で始まるプロパティ名のみで、Java オプションや環境変数とは違う。|
|--properties-file|SparkConf の設定が記述されたファイルのパスを指定。<br>デフォルトは```${SPARK_HOME}/conf/spark-defaults.conf```※ テンプレート有|
|--driver-cores|ドライバに割り当てるコア数（デフォルト1）|
|--driver-memory|ドライバに割り当てるメモリ量（デフォルト1024M）|
|--num-executors|起動するエグゼキュータの数（デフォルト2）|
|--executor-cores|エグゼキュータのコア数（デフォルト1）|
|--executor-memory|エグゼキュータに割り当てるメモリ量（デフォルト1024M）|

--masterの Master URL には次のような値が指定できる。

|Master URL|内容|
|---|---|
|local|ローカルでエグゼキュータに1 つのスレッドを割り当てる。|
|local[n]|ローカルでエグゼキュータに n 個のスレッドを割り当てる。|
|local[*]|ローカルでエグゼキュータにクライアントPCのコア数と同数のスレッドを割り当てる。|
|yarn-client|YARN クラスタに接続。ドライバプログラムがクライアント上で動作。[Running Spark on YARN](http://spark.apache.org/docs/latest/running-on-yarn.html)|
|yarn-cluster|YARN クラスタに接続。ドライバプログラムがクラスタ内の NodeManager 上で動作。[Running Spark on YARN](http://spark.apache.org/docs/latest/running-on-yarn.html)|
|spark://HOST:PORT|Standalone クラスタに接続。デフォルトは7077ポート。[Spark Standalone Mode](https://spark.apache.org/docs/latest/spark-standalone.html)|
|mesos://HOST:PORT|Mesos クラスタに接続。デフォルトは5050ポート。ZooKeeper を使用してる場合はmesos://zk://...になる。[Running Spark on Mesos](https://spark.apache.org/docs/latest/running-on-mesos.html)|


##SparkContextとSparkSession
SparkContextは、RDDを使用する際に生成するインスタンス。
SparkSessionは、Dataset・DataFrameを使用するために生成するインスタンス
SparkSessionインスタンスの生成には、SparkSessionのビルダーを使用する。

```
from pyspark.sql import SparkSession
spark = SparkSession.builder().getOrCreate()
```

|options|内容|
|---|---|
|appName|アプリケーションの名前|
|config|SparkConfの設定オプション。 第一引数がオプション名で、第二引数が実数値|
|enableHiveSupport|apaceh hiveサポートをONにする。|
|getOrCreate|既にあるsessionを取得してオプションを適用するか、新たにsessionを作成する|
|master|動作モード。省略した場合は local モード。spark-submitの物と同一|


SparkContextの機能はSparkSessionに内包されているため、基本的に生成するのはSparkSessionでよい。
```
sc = spark.sparkContext
sc.setCheckpointDir("/tmp/spark/checkpoint")
```

SparkSessionインスタンスを生成すると、HTTPサーバーとしての機能が起動する。
ので、終了するときはstopメソッドを呼び出す。
```
spark = ～
try {
  ～
} finally {
  spark.stop()
}
```

stopすることで、各executorも終了する。


以降プログラム上でのSparkSessionをsparkという変数で表現する。



##DataFrameの生成

###RDDから生成

```
df = spark.createDataFrame(rdd)
```

###ファイルから生成

```
# parquetファイルの場合
df = spark.read.parquet(filePath)
# csvファイルの場合
df = spark.read.csv(filePath)
# jsonファイルの場合
df = spark.read.json(filePath)
```

読み込むファイルにschema情報がない場合、自分でつける必要がある。

```
from pyspark.sql.types import *
struct = StructType([
    StructField('coloumn_name1', StringType(), False),
    StructField('coloumn_name2', StringType(), False)
])
df = spark.read.csv("filepath", struct)
```

複数ファイルを同時に読み込みたいときは、最初に\*をつけてfilepathをlist型にする。<br	>
この時読み込むファイルのschemaが違うとエラーになる。

```
df = spark.read.parquet(*[filepath1, filepath2])
```

##spark SQL

SparkSessionのsqlはアプリケーションがSQLクエリをプログラム的に実行することを可能にできる。
返却値はdataframeとなる。

```
#これでDFをテーブルとして登録する。
df.createOrReplaceTempView("people")

val sqlDF = spark.sql("SELECT * FROM people")
sqlDF.show()
```


###fileからSQLを直接利用する

ファイルからダイレクトに呼べる。

```
df = spark.sql("SELECT * FROM parquet.`examples/src/main/resources/users.parquet`")
```

##dfにmap関数を適用する

dfはrddの機能を使用できる。
その中でよく使ったのがmapだったので記述する。

```
# これで配列に左から二つのカラムを入れたものができる。
data_list = df.rdd.map(lambda x: [x[1], x[0]]).collect()

print(data_list)
```


##dfをファイルに出力する

dfを書き出せる。
ファイルパスで指定したディレクトリに複数に分割されたファイルとして出力される。

```
# parquet
df.write.parquet(ファイルパス)

# csv
df.write.csv(ファイルパス)
```













