import Vue from 'vue'
import Router from 'vue-router'
import List from './views/List.vue'
import Category from './views/Category.vue'
import MdView from './views/MdView.vue'
import Editor from './views/Editor.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'List',
      component: List
    },
    {
      path: '/md_view',
      name: 'md_view',
      component: MdView,
      props: (route) => ({ item: { a: 'a'} })
    },
    {
      path: '/editor',
      component: Editor
    },
    {
      path: '/cat',
      component: Category
    },
  ]
})
