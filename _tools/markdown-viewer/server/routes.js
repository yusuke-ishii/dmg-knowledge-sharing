const controllers = require('./controllers');

const Joi = require('joi')

module.exports = [

  {
    method: 'GET',
    path: '/api/list',
    handler: controllers.markdowns.list,
    config: {
      tags: ['api'],
      description: 'List all markdowns',
      notes: 'List all markdowns'
    }
  },
  {
    method: 'GET',
    path: '/api/get_md',
    handler: controllers.markdowns.getMd,
    config: {
      tags: ['api'],
      description: 'Get a markdown',
      notes: 'Get a markdown',
      validate: {
        query: {
          path: Joi.string()
        }
      }
      // parameters: ['name'],
      // query: ['name']
    }
  },

  // {
  //   method: 'GET',
  //   path: '/api/get/{name}',
  //   handler: controllers.markdowns.getMdByParams
  // },

];
