
const fs = require('fs');
const fl = require('node-filelist');
const path = require('path');

const toRoot = '../../../../';
const rootDir = path.join(__dirname, toRoot);
const ignore = ['.git', '_tools'];


const getMdList = async () => {
  let rootDirs = fs.readdirSync(rootDir);

  rootDirs = rootDirs.filter(d => !ignore.includes(d))
    .map(d => path.join(__dirname, toRoot, d));

  let option = {
    'ext': 'md',
    'isStats': true,
  };

  let flPromise = () => {
    return new Promise( (resolve, reject) => {
      try {
        fl.read(rootDirs, option, (results) => {
          let res = results.map(v => {
            return {
              'path': v.path,
              'mtime': v.stats.mtime,
            };
          });
          resolve(res);
        });
      } catch(e) {
        reject(e);
      }
    });
  };

  let mdList = [];
  try {
    mdList =  await flPromise();
  } catch(e) {
    return e;
  }

  mdList = mdList
    .map(obj => {
      return {
        filename: obj.path.split('/').pop(),
        path: obj.path.replace(rootDir, ''),
        content: fs.readFileSync(obj.path, 'utf8'),
        mtime: obj.mtime,
      };
    })
    .filter(obj => {
      return obj.filename !== 'README.md'
    });

  return mdList;
};



module.exports = {

  list: async (request, reply) => {

    let list = await getMdList();
    return {list: list};
  },


  getMd: async (request, reply) => {
    let mdList = await getMdList();
    let target = request.query.path;
    let targetMd = mdList.filter(md => md.path === target);
    return targetMd[0];
  },

  // getMdByParams: async (request, reply) => {
  //   let mdList = await getMdList();
  //   let target = request.params.name;
  //   let targetMd = mdList.filter(md => md.path === target);
  //   return targetMd[0];
  // },



};