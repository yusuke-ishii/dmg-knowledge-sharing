'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Path = require('path');
const Routes = require('./server/routes');


(async () => {

  const server = await new Hapi.Server({
      host: 'localhost',
      port: 3000,
  });

  async function initWebpackTools (middleware, config) {
    await server.register({
      plugin: middleware,
      options: {
        config: config,
        devOptions: {
          logLevel: 'warn',
          publicPath: config.output.publicPath,
          stats: {
            colors: true
          }
        }
      }
    });
  }

  // Register webpack HMR, fallback to development environment
  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
    const WebpackConfig = require('./config/webpack.config.js'); // Webpack config
    const HapiWebpackMiddleware = require('./server/plugins/HapiWebpackMiddleware');
    initWebpackTools(HapiWebpackMiddleware, WebpackConfig);
  }


  const swaggerOptions = {
    info: {
            title: 'Test API Documentation',
            version: Pack.version,
        },
    };

  await server.register([
    Inert,
    Vision,
    {
        plugin: HapiSwagger,
        options: swaggerOptions
    }
  ]);



  await server.register({
    plugin: Inert
  }).then(() => {
    server.route({
      method: 'GET',
      path: '/assets/{filepath*}',
      config: {
        auth: false,
        cache: {
          expiresIn: 24 * 60 * 60 * 1000,
          privacy: 'public'
        }
      },
      handler: {
        directory: {
          path: Path.join(__dirname, '/public/assets/'),
          listing: false,
          index: false
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/build/{filepath*}',
      config: {
        auth: false,
        cache: {
          expiresIn: 24 * 60 * 60 * 1000,
          privacy: 'public'
        }
      },
      handler: {
        directory: {
          path: Path.join(__dirname, '/public/build/'),
          listing: false,
          index: false
        }
      }
    });


    // Routes.forEach( r => {
    //   server.route(r);
    // })

    server.route(Routes);

    // server.route({
    //   method: 'GET',
    //   path: '/api/list',
    //   config: {
    //       tags: ['api'],
    //       description: 'List all jobs',
    //       notes: 'List all jobs'
    //   }
    // });


    // server.route({
    //   method: 'GET',
    //   path: '/{path*}',
    //   handler: {
    //     file: './public/index.html'
    //   }
    // });


    // server.start();



  });

  try {
    await server.start();
    console.log('Server running at:', server.info.uri);
  } catch(err) {
    console.log(err);
  }

  module.exports = server;

})();