#!/bin/bash

# markdown-viewer起動用シェルスクリプト
# 実行方法: $ sh v

cd _tools/markdown-viewer \
&& npm i \
&& npm run build \
&& node app.js
